# Restaurant website project with CI/CD using Gitlab CI to build and deploy

[![pipeline status](https://gitlab.com/tnphuc1/cicd-web/badges/main/pipeline.svg)](https://gitlab.com/tnphuc1/cicd-web/-/commits/main)


## Project Description

 - Create a restaurant website using HTML, CSS, and Javascript with basic features such as ordering, payment, contact for advice, ...
 - Utilize GitLab CI for source code storage and Docker for building and testing the website before executing CI/CD.
 - Configure CI/CD with GitLab CI tool to automatically build and deploy to staging and production environments.


## Build

- Create a website using HTML, CSS, JavaScript, and JSON if applicable.
- Create two files: *__Dockerfile__* and *__docker-compose.yml__*. Then, proceed to build and test the website:
    ```bash
    docker build -t cicd-web:<tag_name> .
    docker compose up -d
    ```
- Afterward, push the code to GitLab as usual.
- Create a *__.gitlab-ci.yml__* file for GitLab to automatically perform the build.
    ```
    build:
    stage: build
    script:
        - docker pull $CI_REGISTRY_IMAGE:latest || true
        - docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest .
        - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
        - docker push $CI_REGISTRY_IMAGE:latest
    ```

## Authors

- [Tran Nguyen Phuc](https://tnphuc.github.io/Pro_Tran_Nguyen_Phuc)

